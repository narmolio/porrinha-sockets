package src.comum;

/**
 * Um par de valores. Útil para passar parâmetros através de firePropertyChange, que permite apenas um valor.
 */
public class Par {
    public final Object valor1;
    public final Object valor2;

    public Par(Object valor1, Object valor2) {
        this.valor1 = valor1;
        this.valor2 = valor2;
    }
}