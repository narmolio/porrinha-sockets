package src.client;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import src.comum.Par;
import static src.client.TiposEventos.*;
import static java.lang.String.format;

/**
 * Tranforma eventos recebidos em alterações na interface do usuário.
 * É responsável pela inserção de todos os textos visualizados durante a conexão ao servidor e execução do jogo.
 */
public class ChangeListener implements PropertyChangeListener {

    private final Gui gui;
    private String ultimoEvento = "0";
    private final String NOVA_LINHA = System.getProperty("line.separator");

    /**
     * Cria nova instância
     * @param gui interface do usuário
     */
    public ChangeListener(Gui gui) {
        this.gui = gui;        
    }

    // uma chamada a worker.firePropertyChange(...) resulta na chamada deste método
    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        String nome = evt.getPropertyName();
        Par par = null;
        switch(nome) {
            case ID_RECEBIDO: // cliente recebeu um novo id do servidor
                gui.setTitle(format("Jogador %s", evt.getNewValue()));
                break;
            case NOVO_PALPITE: // jogador forneceu um novo palpite
                Object palpite = evt.getNewValue();
                setPalpite(palpite);
                appendInfoJogo("Meu palpite: %d", palpite);
                break;
            case PALPITES_RECEBIDOS: // cliente recebeu novos palpites do servidor
                par = (Par)evt.getNewValue();
                int palpitesFornecidos = (int)par.valor1;
                if(ultimoEvento == NOVO_PALPITE)
                    setInfoJogo("Contagem solicitada...");
                else
                    setInfoJogo("Palpite solicitado...");
                if(palpitesFornecidos > 0) { // exibe palpites recebidos se eles existirem
                    int[] palpites = (int[])par.valor2;
                    String stringPalpites = "Palpites anteriores: ";
                    for(int i = 0; i < palpitesFornecidos; stringPalpites += palpites[i++] + " ");
                    appendInfoJogo(stringPalpites);
                }
                break;
            case NOVA_RODADA:
                setContagem("-");
                setPalpite("-");
                setInfoJogo("");
                break;
            case CONTAGEM: // jogador forneceu uma nova contagem
                Object contagem = evt.getNewValue();
                setContagem(contagem);
                appendInfoJogo("Minha contagem: %d", contagem);
                break;
            case VITORIA: // jogador venceu
                setVitorias(evt.getNewValue());
                appendInfoJogo("Eu venci! \\o/");
                break;
            case INICIANDO_CONEXAO: // cliente está tentando uma nova conexão ao servidor
                gui.getButtonConectar().setEnabled(false);
                par = (Par)evt.getNewValue();
                setLog("Conectando-se a %s:%d", par.valor1, par.valor2);
                break;
            case CONECTADO: // cliente se conectou ao servidor
                appendLog("Conectado");
                setInfoJogo("");
                setVitorias("0");
                gui.getButtonConectar().setEnabled(false);
                gui.getButtonDeconectar().setEnabled(true);
                break;
            case DESCONECTADO: // cliente foi desconectado 
                if(ultimoEvento == ERRO)
                    appendLog("Desconectado");
                else
                    setLog("Desconectado");
                setPalpite("-");
                setContagem("-");
                gui.setTitle("");
                gui.getButtonConectar().setEnabled(true);
                gui.getButtonDeconectar().setEnabled(false);
                break;
            case FIM: // servidor encerrou o jogo
                setInfoJogo("Fim de jogo");
                break;
            case ERRO: // ocorreu um erro no socket do cliente (servidor foi encerrado ou o cliente desconectado enquanto fazia leitura/escrita)
                setInfoJogo("");
                if(ultimoEvento == DESCONECTADO)
                    appendLog(evt.getNewValue().toString());
                else
                    setLog(evt.getNewValue().toString());
                break;
            case SEM_VENCEDOR: // servidor avisou que ninguém venceu
                appendInfoJogo("Ninguém venceu");
                break;
            case OUTRO_VENCEDOR: // outro jogador venceu
                appendInfoJogo("Jogador %d venceu :|", evt.getNewValue());
                break;
            case ERRO_CONEXAO: // falha ao tentar uma conexão
                setLog(evt.getNewValue().toString());
                gui.getButtonConectar().setEnabled(true);
                gui.getButtonDeconectar().setEnabled(false);                
                break;
            default:
                break;
        }
        ultimoEvento = nome;
    }

    /**
     * Põe um valor do campo de texto palpite
     * @param valor valor do palpite
     */
    private void setPalpite(Object valor) {
        gui.getTextFieldPalpite().setText(valor.toString());
    }
    
    /**
     * Põe um valor no campo de texto contagem
     * @param valor valor da contagem
     */
    private void setContagem(Object valor) {
        gui.getTextFieldContagem().setText(valor.toString());
    }

    /**
     * Põe um valor no campo de texto vitorias
     * @param valor quantidade de vitórias
     */
    private void setVitorias(Object valor) {
        gui.getTextFieldVitorias().setText(valor.toString());
    }

    /**
     * Sobrescreve todo o conteúdo da segunda área de texto
     * @param formato
     * @param args
     */
    private void setLog(String formato, Object... args) {
        gui.getTextAreaLog().setText(format(formato, args));
    }

    /**
     * Sobrescreve todo o conteúdo da primeira área de texto 
     * @param formato
     * @param args
     */
    private void setInfoJogo(String formato, Object... args) {
        gui.getTextAreaInfoJogo().setText(format(formato, args));
    }

    /**
     * Adiciona uma linha de texto na segunda área de texto
     * @param formato
     * @param args
     */
    private void appendLog(String formato, Object... args) {
        gui.getTextAreaLog().append(NOVA_LINHA + format(formato, args));
    }

    /**
     * Adiciona uma linha de texto na primeira área de texto
     * @param formato
     * @param args
     */
    private void appendInfoJogo(String formato, Object... args) {
        gui.getTextAreaInfoJogo().append(NOVA_LINHA + format(formato, args));
    }
}