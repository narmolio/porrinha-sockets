package src.client;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.Border;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.BorderLayout;
import java.awt.Color;


/**
 * Cria a janela, botões, labels, etc.
 */
public class Gui extends JFrame {

    private static final long serialVersionUID = 1L; // irrelevante, apenas para retirar um alerta da IDE

    private JTextField textFieldHost;
    private JTextField textFieldPorta;
    private JTextField textFieldDelay;
    private JTextField textFieldPalpite;
    private JTextField textFieldContagem;
    private JTextField textFieldVitorias;
    private JTextArea textAreaInfoJogo; // primeira área de texto, com informações do jogo
    private JTextArea textAreaLog; // segunda área de texto, com mensagens de erro e informações de conexão
    private JButton buttonConectar;
    private JButton buttonDeconectar;

    public Gui() {
        iniciar();
    }

    private void iniciar() {
        setResizable(false);
        criarComponentes();
    }

    private void criarComponentes() {
        Container contentPane = getContentPane();
        JPanel panel1 = new JPanel(); // recebe: host, porta, delay
        JPanel panel2 = new JPanel(); // recebe: conectar, desconectr
        JPanel panel3 = new JPanel(); // recebe: palpite, contagem, vitórias
        JPanel raiz = new JPanel(); // recebe: paineis anteriores e áreas de texto
        JLabel labelHost = new JLabel("Host");
        JTextField textFieldHost = new JTextField("", 10);
        JLabel labelPorta = new JLabel("Porta");
        JTextField textFieldPorta = new JTextField("", 5);
        JLabel labelDelay = new JLabel("Delay");
        JTextField textFieldDelay = new JTextField("", 4);
        JButton buttonConectar = new JButton("Conectar");
        JButton buttonDeconectar = new JButton("Desconectar");
        JLabel labelPalpite = new JLabel("Palpite");
        JTextField textFieldPalpite = new JTextField("-", 3);
        JLabel labelContagem = new JLabel("Contagem");
        JTextField textFieldContagem = new JTextField("-", 3);
        JLabel labelVitorias = new JLabel("Vitórias");
        JTextField textFieldVitorias = new JTextField("0", 3);
        JTextArea textAreaInfoJogo = new JTextArea(6, 20);
        JTextArea textAreaLog = new JTextArea(3, 20);
        JScrollPane scrollInfoJogo = new JScrollPane(textAreaInfoJogo);
        Dimension tamanhoTextArea = new Dimension(330, 200);
        Dimension tamanhoRigidArea = new Dimension(0, 5);
        Border bordaExternaTextArea = BorderFactory.createEtchedBorder();
        Border bordaInternaTextArea = BorderFactory.createEmptyBorder(5, 5, 5, 5);
        Border bordaTextArea = BorderFactory.createCompoundBorder(bordaExternaTextArea, bordaInternaTextArea);
        this.textFieldHost = textFieldHost;
        this.textFieldPorta = textFieldPorta;
        this.textFieldDelay = textFieldDelay;
        this.buttonConectar = buttonConectar;
        this.buttonDeconectar = buttonDeconectar;
        this.textFieldContagem = textFieldContagem;
        this.textFieldPalpite = textFieldPalpite;
        this.textFieldVitorias = textFieldVitorias;
        this.textAreaInfoJogo = textAreaInfoJogo;
        this.textAreaLog = textAreaLog;
        raiz.setLayout(new BoxLayout(raiz, BoxLayout.Y_AXIS)); // dispõe os componentes verticalmente
        textFieldPalpite.setEditable(false);        
        textFieldContagem.setEditable(false);        
        textFieldVitorias.setEditable(false);
        textFieldPalpite.setHorizontalAlignment(JTextField.CENTER);
        textFieldContagem.setHorizontalAlignment(JTextField.CENTER);
        textFieldVitorias.setHorizontalAlignment(JTextField.CENTER);
        textAreaInfoJogo.setMaximumSize(tamanhoTextArea);
        textAreaLog.setMaximumSize(tamanhoTextArea);
        textAreaLog.setBorder(bordaTextArea);
        scrollInfoJogo.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scrollInfoJogo.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);
        scrollInfoJogo.setMaximumSize(tamanhoTextArea);
        scrollInfoJogo.setBorder(bordaTextArea);
        scrollInfoJogo.setBackground(textAreaInfoJogo.getBackground());
        textAreaInfoJogo.setEditable(false);
        textAreaLog.setEditable(false);
        textAreaInfoJogo.setForeground(new Color(0, 51, 0));
        textAreaLog.setForeground(Color.BLUE);
        panel1.add(labelHost);
        panel1.add(textFieldHost);
        panel1.add(labelPorta);
        panel1.add(textFieldPorta);
        panel1.add(labelDelay);
        panel1.add(textFieldDelay);
        panel2.add(buttonConectar);
        panel2.add(buttonDeconectar);
        panel3.add(labelPalpite);
        panel3.add(textFieldPalpite);
        panel3.add(labelContagem);
        panel3.add(textFieldContagem);
        panel3.add(labelVitorias);
        panel3.add(textFieldVitorias);
        raiz.add(panel1);
        raiz.add(panel2);
        raiz.add(panel3);
        raiz.add(Box.createRigidArea(tamanhoRigidArea)); // adiciona um espaço entre os componentes
        raiz.add(scrollInfoJogo);
        raiz.add(Box.createRigidArea(tamanhoRigidArea));
        raiz.add(textAreaLog);
        raiz.add(Box.createRigidArea(tamanhoRigidArea));
        contentPane.add(raiz, BorderLayout.CENTER);
        pack(); // compacta a janela com apenas o espaço necessário para os componentes
    }

    public void exibir() {
        setVisible(true);
    }

    public JTextField getTextFieldHost() {
        return textFieldHost;
    }

    public JTextField getTextFieldPorta() {
        return textFieldPorta;
    }

    public JTextField getTextFieldDelay() {
        return textFieldDelay;
    }

    public JTextField getTextFieldPalpite() {
        return textFieldPalpite;
    }

    public JTextField getTextFieldContagem() {
        return textFieldContagem;
    }

    public JTextField getTextFieldVitorias() {
        return textFieldVitorias;
    }

    public JTextArea getTextAreaInfoJogo() {
        return textAreaInfoJogo;
    }

    public JTextArea getTextAreaLog() {
        return textAreaLog;
    }

    public JButton getButtonConectar() {
        return buttonConectar;
    }

    public JButton getButtonDeconectar() {
        return buttonDeconectar;
    }

}