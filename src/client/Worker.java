package src.client;

import javax.swing.SwingWorker;
import static src.client.TiposEventos.ERRO_CONEXAO;

/**
 * Esta classe é necessária para a realização de tarefas externas à thread principal do swing
 * que façam alterações nos componentes da interface do usuário.
 * Uma tarefa muito demorada precisa ser executada fora da thread principal do swing, 
 * caso contrário a janela congelará.
 * A longa tarefa aqui é comunicação do cliente com o servidor, executada em pelo método iniciar() de Cliente.
 * A execução da tarefa em doInBackground() é iniciada com a chamada do método execute().
 * O objeto não pode ser reutilizada, cada chamada a execute() demanda uma nova instância.
 */
public class Worker extends SwingWorker<Void, Void> {

    private final Cliente cliente;
    private final String host;
    private final int porta;

    /**
     * Cria uma nova instância
     * @param cliente înstância que faz a comunicação com o servidor
     * @param host host do socket ao qual o cliente se conectará; pode ser o ip ou nome
     * @param porta porta do socket ao qual o cliente se conectará
     */
    public Worker(Cliente cliente, String host, int porta) {
        this.cliente = cliente;
        this.host = host;
        this.porta = porta;
    }

    /**
     * Em relação a thread principal do swing este método é assíncrono, mas os eventos disparados e tratados
     * em ChangeListener, não; estes são executados na thread principal do swing
     */
    @Override
    protected Void doInBackground() throws Exception {
        if(conectarCliente())
            iniciarCliente();
        return null;
    }
    
    /**
     * Faz a conexão do cliente com o socket servidor
     * @return true se a conexão for bem sucedida, false caso contrário
     */
    private boolean conectarCliente() {
        try {
            cliente.conectar(host, porta);
        }
        catch(Exception e) {
            firePropertyChange(ERRO_CONEXAO, null, "Não foi possível se conectar ao servidor"); // será tratado em ChangeListener
            return false;
        }
        return true;
    }

    /**
     * Inicia a comunicação do cliente como o servidor
     */
    private void iniciarCliente() {
        try { cliente.iniciar(); }
        catch (Exception e) { }
    }    
}