package src.client;

/**
 * Ponto de entrada. Apenas cria uma instância de App e inicia o programa.
 */
public class Main {    
    public static void main(String[] args) {
        new App().iniciar();
    }
}