package src.client;

import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

/**
 * Esta classe reage a mudanças em um JTextField, fazendo validação de seu texto.
 * O texto deve ser uma string conversível para long e que represente um valor menor que delayMaximo.
 * Se a string for validada o valor convertido será posto como valor de delay em uma instância de Cliente.
 * Caso contrário, o valor do JTextField será restaurado com o valor presente em Cliente
 */
public class DelayListener implements DocumentListener, Runnable {

    private Cliente cliente;
    private long delayMaximo;
    private JTextField textFieldDelay;

    /**
     * Cria uma nova instância
     * @param gui instância que possui o JTextField a ser observado
     * @param cliente receberá o delay
     * @param delayMaximo valor máximo do JTextField
     */
    public DelayListener(Gui gui, Cliente cliente, long delayMaximo) {
        this.cliente = cliente;
        this.delayMaximo = delayMaximo;
        textFieldDelay = gui.getTextFieldDelay();
    }


    @Override
    public void insertUpdate(DocumentEvent e) {
        SwingUtilities.invokeLater(this);
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        SwingUtilities.invokeLater(this);
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
        SwingUtilities.invokeLater(this);
    }

    /**
     * Chamado sempre que há uma modificação no campo de texto.
     * O método é chamado na thread principal do swing.
     */
    @Override
    public void run() {
        String delayStr = textFieldDelay.getText();
        if(delayStr.isEmpty()) {
            cliente.setDelay(0);
            textFieldDelay.setText("0");
            return;
        }
        try {
            long delay = Long.parseLong(delayStr);
            if(delay > delayMaximo)
                restaurar();
            else
                cliente.setDelay(delay);
        }
        catch(NumberFormatException e) {
            restaurar();
        }
    }
    
    private void restaurar() {
        textFieldDelay.setText(Long.toString(cliente.getDelay())); 
    }
}