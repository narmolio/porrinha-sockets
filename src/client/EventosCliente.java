package src.client;

/**
 * Descreve eventos disparados pela classe Cliente
 */
public interface EventosCliente {
    /**
     * Chamado quando uma tentativa de conexão é iniciada
     * @param host ip ou nome do socket servidor
     * @param porta porta do socket servidor
     */
    public void iniciandoConexao(String host, int porta);

    /**
     * Chamado quando uma tentativa de conexão foi bem sucedida
     */
    public void conectado();

    /**
     * Chamado quando a instância de {@link Cliente} foi desconectada
     */
    public void desconectado();

    /**
     * Chamado quando o socket servidor encerra o jogo
     */
    public void fimJogo();

    /**
     * Chamado quando o cliente envia uma mensagem
     * @param msg mensagem enviada por a instância de {@link Cliente}
     */
    public void mensagem(String msg);

    /**
     * Chamado quando ocorre um erro durante a execução do jogo
     * @param msg mensagem descrevendo o erro
     */
    public void erro(String msg);

    /**
     * Chamado quando a conexão é bem sucedida e o socket servidor ainda esperava jogadores
     * @param id id recebido do socket servidor
     */
    public void idRecebido(int id);

    /**
     * Chamado quando a instância de {@link Jogador} fornece um palpite
     * @param valor palpite fornecido por a instância de {@link Jogador}. Este valor será enviado para o servidor
     */
    public void novoPalpite(int valor);

    /**
     * <p>Chamado sempre que o cliente recebe um conjunto de palpites.</p>
     * <p>Os palpites fornecidos por outros jogadores estão localizados no intervalo de índice [0, palpitesFornecidos - 1]</p>
     * <p>Os palpites disponíveis para escolha estão localizados no intervalo de índice [palpitesFornecidos, palpites.length - 1]</p>
     * @param palpitesFornecidos quantidade de palpites fornecidos por outros jogadores
     * @param palpites conjunto de palpites já fornecidos e disponíveis
     */
    public void palpitesRecebidos(int palpitesFornecidos, int[] palpites);

    /**
     * Chamado quando a instância de {@link Jogador} fornece uma contagem
     * @param valor contagem fornecido pela instância de {@link Jogador}. Este valor será enviado para o servidor
     */
    public void novaContagem(int valor);

    /**
     * Chamado quando a instância de {@link Jogador} vence a rodada.
     * @param quantidade quantidade de vitórias atualizada
     */
    public void vitoria(int quantidade);

    /**
     * Chamado quando nenhum jogador venceu a rodada
     */
    public void semVencedor();

    /**
     * Chamado quando outro jogador venceu a rodada
     * @param id id do jogador venceu
     */
    public void outroVencedor(int id);    

    public void novaRodada();
}