package src.client;

/**
 * Eventos disparados no SwingWorker.
 * Os nomes das vaiáveis são importantes, seus valores não.
 */
public class TiposEventos {
    public static final String ID_RECEBIDO = "a";
    public static final String NOVO_PALPITE = "b";
    public static final String PALPITES_RECEBIDOS = "c";
    public static final String CONTAGEM = "d";
    public static final String VITORIA = "e";
    public static final String INICIANDO_CONEXAO = "f";
    public static final String CONECTADO = "g";
    public static final String DESCONECTADO = "h";
    public static final String FIM = "i";
    public static final String MENSAGEM = "j";
    public static final String ERRO = "k";
    public static final String SEM_VENCEDOR = "l";
    public static final String OUTRO_VENCEDOR = "m";
    public static final String ERRO_CONEXAO = "n";
    public static final String NOVA_RODADA = "o";
}