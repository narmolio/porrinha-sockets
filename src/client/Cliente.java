package src.client;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

/**
 * Encapsula um socket que se conecta, envia e recebe valores do servidor;
 * Os dados enviados ao servidor estão no formato [byte int int], onde byte descreve o que está sendo enviado, int é o id do Jogador, 
 * e int é em valor de palpite ou contagem fornecido por Jogador.
 */
public class Cliente {
    private Socket socket = null; // socket
    private DataInputStream entrada; // escreve dados para o servidor
    private DataOutputStream saida; // lê dados do sservidor
    private Jogador jogador; // instância de um Jogador
    private EventosCliente eventos; // eventos que serão disparados
    private long delay = 0; // delay para melhor visualização dos eventos

    private static final byte CODIGO_ID = 1; // servidor diz que está enviando o id do jogador
    private static final byte CODIGO_PALPITE = 2; // servidor diz que está solicitanto um palpite; este socket diz que está fornecendo o palpite 
    private static final byte CODIGO_CONTAGEM = 3; // servidor diz que está solicitando uma contagem; este socket diz que está fornecendo a contagem
    private static final byte CODIGO_VENCEDOR = 4; // servidor diz o id jo jogador vencedor
    private static final byte CODIGO_SAIDA = 5; // servidor diz que o jogo está encerrado 
    private static final byte NOVA_RODADA = 6;

    /**
     * Cria um Cliente sem se conectar ao servidor
     * @param jogador instância que implementa a lógica de um jogador de porrinha
     * @param eventos instância que interpretará eventos disparados
     */
    public Cliente(Jogador jogador, EventosCliente eventos) {
        this.jogador = jogador;
        this.eventos = eventos;
    }

    /**
     * Cria um Cliente e se conecta ao servidor
     * @param host ip ou nome do socket servidor
     * @param porta porta do socket servidor
     * @param jogador instância que implementa a lógica de um jogador de porrinha
     * @param eventos instância que interpretará eventos disparados
     * @throws IOException em caso de erro ao criar o socket ou erro ao criar os streams
     */
    public Cliente(String host, int porta, Jogador jogador, EventosCliente eventos) throws IOException {        
        this.eventos = eventos;
        this.jogador = jogador;
        this.conectar(host, porta);
    }
    
    /**
     * pausa a execução se o valor de delay é maior que zero
     */
    private void sleep() {
        if(delay > 0)
            try { Thread.sleep(delay); }
            catch(Exception e) {}
    }

    /**
     * Lê os palpites fornecidos pelo servidor
     * @return array de palpites
     * @throws IOException em caso de erro de leitura
     */
    private int[] getPalpites() throws IOException {
        int tamanhoArray = entrada.readInt();
        int[] palpites = new int[tamanhoArray];
        for(int i = 0; i < tamanhoArray; ++i)
            palpites[i] = entrada.readInt();
        return palpites;
    }

    /**
     * Envia os parâmetros fornecidos para o servidor
     * @param codigo diz o que significa o parâmetro valor
     * @param id id do jogador
     * @param valor contagem ou palpites
     * @throws IOException em caso de erro de escrita
     */
    private void enviar(byte codigo, int id, int valor) throws IOException {
        saida.writeByte(codigo);
        saida.writeInt(id);
        saida.writeInt(valor);
        saida.flush();
    }

    /**
     * Faz a conexão com um socket servidor
     * @param host ip ou nome do socket servidor
     * @param porta porta do socket servidor
     * @throws IOException em caso de erro ao criar o socket ou erro ao criar os streams
     */
    public void conectar(String host, int porta) throws IOException {
        desconectar();
        eventos.iniciandoConexao(host, porta);
        socket = new Socket(host, porta);
        entrada = new DataInputStream(socket.getInputStream());
        saida = new DataOutputStream(socket.getOutputStream());
        eventos.conectado();
    }

    /**
     * Fecha o socket. Ocorrerá uma exceção se o método iniciar estiver sendo executado
     * @throws IOException em caso de erro de entrada/saida
     */
    public void desconectar() throws IOException {
        if(socket != null) {
            eventos.desconectado();
            entrada.close();
            saida.close();
            socket.close();
            socket = null;
        }
    }

    /**
     * O Cliente inicia a sua comunicação com servidor e com a instância de Jogaodor.
     * A execução permenece neste método até que uma exceção seja disparada ou o servidor envie um CODIGO_SAIDA
     * @throws IOException em caso de erro de leitura ou escrita
     * @throws Exception em caso de código desconhecido
     */
    public void iniciar() throws IOException, Exception {
        int[] palpites;
        int palpitesFornecidos;
        byte codigo = 0;
        try {
            while(codigo != CODIGO_SAIDA) {
                codigo = entrada.readByte();
                switch(codigo) {
                    case CODIGO_ID: // quando o servidor fornece um id; executado uma vez por jogo
                        jogador.setId(entrada.readInt());
                        eventos.idRecebido(jogador.getId());
                        break;
                    case CODIGO_PALPITE: // servidor solicita um palpite
                        palpites = getPalpites();
                        palpitesFornecidos = entrada.readInt();
                        int palpite = jogador.getPalpite(palpitesFornecidos, palpites);
                        sleep();
                        eventos.palpitesRecebidos(palpitesFornecidos, palpites);
                        sleep();
                        eventos.novoPalpite(palpite);
                        enviar(codigo, jogador.getId(), palpite);
                        break;
                    case CODIGO_CONTAGEM: // servidor solicita uma contagem
                        palpites = getPalpites();
                        palpitesFornecidos = entrada.readInt();
                        int contagem = jogador.getContagem(palpitesFornecidos, palpites);
                        sleep();
                        eventos.palpitesRecebidos(palpitesFornecidos, palpites);
                        sleep();
                        eventos.novaContagem(contagem);
                        enviar(codigo, jogador.getId(), contagem);
                        break;
                    case CODIGO_VENCEDOR: // servidor avisa que terminou uma rodada e existe um id vencedor
                        int idVencedor = entrada.readInt();
                        if(jogador.alertarVencedor(idVencedor))
                            eventos.vitoria(jogador.getQuantidadeVitorias()); // esta instância de jogador venceu
                        else if(idVencedor == -1)
                            eventos.semVencedor(); // -1 indica que ninguém acertou o palpite
                        else
                            eventos.outroVencedor(idVencedor);
                        break;
                    case NOVA_RODADA:
                        eventos.novaRodada();
                        break;
                    case CODIGO_SAIDA: // fim de jogo
                        eventos.mensagem("Total de vitórias: " + jogador.getQuantidadeVitorias());
                        eventos.fimJogo();
                        break;
                    default:
                        throw new Exception("O código " + codigo + " é desconhecido");
                }
            }    
        }
        catch(Exception e) { // ocorre se socket é fechado ou o servidor é desconectado sem enviar CODIGO_SAIDA
            eventos.erro("Não foi possível se comunicar com o servidor");
            throw e;
        }
        finally {
            desconectar();
        }
    }

    /**
     * Coloca um novo valor de delay
     * @param delay novo valor do delay
     */
    public void setDelay(long delay) {
        this.delay = delay;
    }

    /**
     * Retorna o valor de delay
     * @return delay do cliente
     */
    public long getDelay() {
        return delay;
    }

}