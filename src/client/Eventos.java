package src.client;

import javax.swing.SwingWorker;
import src.comum.Par;
import static src.client.TiposEventos.*;

/**
 * Recebe os eventos disparados por Cliente e os repassa para o SwingWorker.
 * O tratamento destes eventos resultará em mudanças na interface do cliente.
 */
public class Eventos implements EventosCliente {

    // instância de SwingWorker para onde os eventos serão repassados
    private SwingWorker<Void, Void> worker;

    /**     
     * @param worker instância do SwingWorker para onde os eventos serão repassados
     */
    public void setSwingWorker(SwingWorker<Void, Void> worker) {
        this.worker = worker;
    }

    /*
        Os métodos abaixo repassam os eventos para o SwingWorker resultando em mudanças na interface do usuário.
        Sobre os parâmetros de firePropertyChange:
            1º - nome da propriedade modificada; aqui pode ser interpretado como um evento ocorrido
            2º - o valor antigo; não utilizado aqui
            3º - novo valor; aqui usado para passar valores importantes para quem vai tratar o evento
        A chamada de qualquer um desses métodos resulta na chamada de propertyChange em ChangeListener
     */

    @Override
    public void idRecebido(int id) {
        worker.firePropertyChange(ID_RECEBIDO, null, id);
    }

    @Override
    public void novoPalpite(int valor) {
        worker.firePropertyChange(NOVO_PALPITE, null, valor);
    }

    @Override
    public void palpitesRecebidos(int palpitesFornecidos, int[] palpites) {
        worker.firePropertyChange(PALPITES_RECEBIDOS, null, new Par(palpitesFornecidos, palpites));
    }

    @Override
    public void novaContagem(int valor) {
        worker.firePropertyChange(CONTAGEM, null, valor);
    }

    @Override
    public void vitoria(int quantidade) {
        worker.firePropertyChange(VITORIA, null, quantidade);
    }

    @Override
    public void iniciandoConexao(String host, int porta) {
        worker.firePropertyChange(INICIANDO_CONEXAO, null, new Par(host, porta));
    }

    @Override
    public void conectado() {
        worker.firePropertyChange(CONECTADO, null, "conectado");
    }

    @Override
    public void desconectado() {
        worker.firePropertyChange(DESCONECTADO, null, "desconectado");
    }

    @Override
    public void fimJogo() {
        worker.firePropertyChange(FIM, null, "fim");
    }

    @Override
    public void mensagem(String msg) {
        worker.firePropertyChange(MENSAGEM, null, msg);
    }

    @Override
    public void erro(String msg) {
        worker.firePropertyChange(ERRO, null, msg);
    }

    @Override
    public void semVencedor() {
        worker.firePropertyChange(SEM_VENCEDOR, null, "sem vitoria");

    }

    @Override
    public void outroVencedor(int id) {
        worker.firePropertyChange(OUTRO_VENCEDOR, null, id);
    }

    @Override
    public void novaRodada() {
        worker.firePropertyChange(NOVA_RODADA, null, "nova rodada");
    }
    
}