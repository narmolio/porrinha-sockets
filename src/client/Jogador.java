package src.client;

import java.util.Random;

/**
 * Encapsula a lógica de um jogador de porrinha.
 * Lógica que aqui não existe, o jogador é burro e faz escolhas puramente aleatórias.
 */
public class Jogador {
    private int id = -1; // id será fornecido por Cliente
    private int quantidadeVitorias = 0; // número de vitórias durante um jogo
    private Random random = new Random(); // para gerar números aleatórios

    private static final int CONTAGEM_INDIVIDUAL_MAXIMA = 3; // a escolha da contagem está no intervalo [0, 3]

    /**
     * Coloca um id neste jogador
     * @param id valor do id fornecido
     */
    public void setId(int id) {
        this.id = id;
    };

    /**
     * Retorna o id do jogador
     * @return o id do jogador
     */
    public int getId() {
        return id;
    }

   /**
     * <p>O jogador escolherá um palpite entre os disponíveis</p>
     * <p>Os palpites fornecidos por outros jogadores estão localizados no intervalo de índice [0, palpitesFornecidos - 1]</p>
     * <p>Os palpites disponíveis para escolha estão localizados no intervalo de índice [palpitesFornecidos, palpites.length - 1]</p>
     * @param palpitesFornecidos quantidade de palpites fornecidos por outros jogadores
     * @param palpites conjunto de palpites já fornecidos e disponíveis
     * @return o palpite escoolhido pelo jogador
     */
    public int getPalpite(int palpitesFornecidos, int[] palpites) {
        int indice = random.nextInt(palpites.length - palpitesFornecidos) + palpitesFornecidos;
        return palpites[indice];
    }

    /**
     * Fornece uma contagem que será somada ao total do jogo. 
     * @param palpitesFornecidos quantidade de palpites fornecidos por outros jogadores
     * @param palpites conjunto de palpites já fornecidos e disponíveis
     * @return a contagem escolhida pelo jogador. O Valor está no intervalo [0, 3]
     */
    public int getContagem(int palpitesFornecidos, int[] palpites) { // os palpites poderiam ser usados para fornecer um valor mais inteligente
        return random.nextInt(CONTAGEM_INDIVIDUAL_MAXIMA + 1);
    }

    /**
     * Fornece o total de vitórias
     * @return o total de vitórias
     */
    public int getQuantidadeVitorias() {
        return quantidadeVitorias;
    }

    /**
     * Fornece um aviso sobre o vencedor da rodada
     * @param id id do vencedor
     * @return true se este jogador venceu, falso caso contrário
     */
    public boolean alertarVencedor(int id) {
        if(this.id == id) {
            ++quantidadeVitorias;
            return true;
        }
        return false;
    }

    /**
     * Fornece uma quantidade de vitóras
     * @param valor quantidade de vitórias
     */
    public void setQuantidadeVitorias(int valor) {
        quantidadeVitorias = valor;
    }
}