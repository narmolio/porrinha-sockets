package src.client;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import java.awt.event.ActionEvent;

/**
 * Funciona como um controller. Inicia a interface do usuário, cria eventos de botões e demais objetos.
 */
public class App implements Runnable {
    
    private final long DELAY_MAXIMO = 5000; // valor máximo do campo de texto delay
    private final long DELAY_PADRAO = 1500; // valor inicial do campo de texto delay
    private final String HOST_PADRAO = "localhost"; // valor inicial do campo de texto host
    private final String PORTA_PADRAO = "42426"; // valor incial do campo de texto porta

    private Eventos eventos = new Eventos(); // trata disparados por Cliente
    private Jogador jogador = new Jogador(); // implementa a lógica de um jogador de porrinha
    private Cliente cliente = new Cliente(jogador, eventos); // encapsula operações de sockets
    private Gui gui;    // interface do usuário
    private ChangeListener changeListener; // trata eventos disparados no SwingWorker (variável worker)
    private Worker worker = null; // (SwingWorker) necessário para threads externas à thread principal do swing

    /**
     * Método chamado na thread do principal do swing. Instancia a janela, demais componentes e trata seus eventos.
     */
    @Override
    public void run() {
        gui = new Gui(); // instancia a janela e demais componentes
        changeListener = new ChangeListener(gui); // instancia o manipulador de eventos do SwingWorker
        cliente.setDelay(DELAY_PADRAO); // configura o delay incial do cliente

        gui.getButtonDeconectar().setEnabled(false);

        gui.getTextFieldHost().setText(HOST_PADRAO); // põe o valor no campo de texto host
        gui.getTextFieldPorta().setText(PORTA_PADRAO); // põe o valor no campo de texto porta
        gui.getTextFieldDelay().setText(Long.toString(DELAY_PADRAO)); // põe o valor no campo de texto delay

        // muda o valor do delay no cliente a cada mudança no campo de texto. Ver DelayListener.java
        gui.getTextFieldDelay().getDocument().addDocumentListener(new DelayListener(gui, cliente, DELAY_MAXIMO));

        // adiciona uma ação ou evento a ser executado no click do botão conectar
        gui.getButtonConectar().addActionListener((ActionEvent evt) -> {
            try {
                if(worker == null || worker.isDone()) { // não faz nada se o SwingWorker ainda estiver realizando uma tarefa
                    jogador.setQuantidadeVitorias(0); // reinicia o contador de vitórias do jogador
                    worker = new Worker(cliente, getHost(), getPorta()); // instancia um novo SwingWorker (esta classe não permite reutilização do objeto)
                    worker.addPropertyChangeListener(changeListener); // adiciona o manipulador de eventos a nova instância
                    eventos.setSwingWorker(worker); // manipulador de eventos do cliente recebe a nova instância do SwingWorker
                    worker.execute(); // inicia a execução da tarefa. A execução é assíncrona.
                }    
            }
            catch(NumberFormatException e) { // disparada por getPorta() caso o valor do campo porta não possa ser convertido para um valor inteiro
                gui.getTextAreaLog().setText("'Porta' precisa ser um inteiro no intervalo [1, 65535]");
            }
            catch(Exception e) {                                // disparada por getPorta() se o inteiro convertido não estiver no intervalo [1, 65535]                                                                                        
                gui.getTextAreaLog().setText(e.getMessage());   // disparada por getHost() se o campo host estiver vazio
            }
        });

        // adiciona uma ação ou evento a ser executado no click do botão desconectar
        gui.getButtonDeconectar().addActionListener((ActionEvent evt) -> {
            desconectarCliente();
        });
        
        // a ser executado quando a aplicação fechar. Desconecta o cliente.
        Runtime.getRuntime().addShutdownHook(new Thread() {            
            @Override
            public void run() { desconectarCliente(); }
        });

        gui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // diz ao JFrame que a janela deve ser fechada ao clicar no botão... fechar
        gui.exibir(); // exibe a janela
    }

    /**
     * Invoca o método run na thread principal do swing, mostrando a janela e iniciando a aplicação.
     */
    public void iniciar() {
        SwingUtilities.invokeLater(this);
    }

    /**
     * Retorna a String contida no campo host
     * @return a string contida no campo host
     * @throws Exception caso o campo host esteja vazio
     */
    private String getHost() throws Exception {
        String host = gui.getTextFieldHost().getText().trim();
        if(host.isEmpty())
            throw new Exception("Campo 'Host' não preenchido");
        return host;
    }

    /**
     * Retorna o inteiro contido no campo porta
     * @return o valor inteiro contido no campo porta
     * @throws NumberFormatException caso o texto do campo host não possa ser convertido para um inteiro
     * @throws Exception caso o inteiro convertido não esteja no intervalo [1, 65535]
     */
    private int getPorta() throws NumberFormatException, Exception {
        int porta = Integer.parseInt(gui.getTextFieldPorta().getText().trim());
        if(porta < 1 || porta > 65535)
            throw new Exception("'Porta' precisa ser um inteiro no intervalo [1, 65535]");
        return porta;
    }

    /**
     * Desconecta a instância de {@link Cliente}, o que basicamente significa fechar o socket encapsulado.
     */
    private void desconectarCliente() {
        try {
            cliente.desconectar();
        }
        catch(Exception e) {}        
    }
}