package src.server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import static java.lang.String.format;

public class JogadorRemoto {
    private Socket socket;
    private final int ID;
    private int palpite = -1;
    private int contagem = -1;
    private int quantidadeVitorias = 0;
    private DataOutputStream saida;
    private DataInputStream entrada;

    private static final byte CODIGO_ID = 1;
    private static final byte CODIGO_PALPITE = 2;
    private static final byte CODIGO_CONTAGEM = 3;
    private static final byte CODIGO_VENCEDOR = 4;
    private static final byte CODIGO_SAIDA = 5;
    private static final byte NOVA_RODADA = 6;

    public JogadorRemoto(Socket socket, int id) throws IOException{
        this.socket = socket;
        ID = id;
        saida = new DataOutputStream(socket.getOutputStream());
        entrada = new DataInputStream(socket.getInputStream());
        enviarId();
    }

    private void enviarId() throws IOException {
        saida.write(CODIGO_ID);
        saida.writeInt(ID);
        saida.flush();
    }

    private void enviarDados(byte codigo, int palpitesFornecidos, int[] palpites) throws IOException {
        saida.writeByte(codigo);
        saida.writeInt(palpites.length);       
        for(int palpite : palpites)
            saida.writeInt(palpite);
        saida.writeInt(palpitesFornecidos);            
        saida.flush();
    }

    private void testarDadosRecebidos(int codigoEsperado) throws Exception, IOException {
        final byte codigo = entrada.readByte();
        final int id = entrada.readInt();
        if(id != this.ID)
            throw new Exception(format("O id %d recebido é inválido. Id esperado: %d", id, ID));
        if(codigo != codigoEsperado)
            throw new Exception(format("O código %d recebido é inválido. Código esperado: %d", codigo, codigoEsperado));
    }

    public void encerrarConexao() throws IOException {
        saida.writeByte(CODIGO_SAIDA);
        saida.flush();
        entrada.close();
        saida.close();
        socket.close();
    }

    public int getPalpite(int palpitesFornecidos, int[] palpites) throws Exception, IOException {
        enviarDados(CODIGO_PALPITE, palpitesFornecidos, palpites);
        testarDadosRecebidos(CODIGO_PALPITE);
        this.palpite = entrada.readInt();
        return this.palpite;
    }

    public int getUltimoPalpite() {
        return palpite;
    }

    public int getUltimaContagem() {
        return contagem;        
    }

    public int getQuantidadeVitorias() {
        return quantidadeVitorias;
    }

    public int getContagem(int palpitesFornecidos, int[] palpites) throws Exception, IOException {
        enviarDados(CODIGO_CONTAGEM, palpitesFornecidos, palpites);
        testarDadosRecebidos(CODIGO_CONTAGEM);
        contagem = entrada.readInt();
        return contagem;
    }

    public void alertarVencedor(int id) throws IOException {
        if(id == ID)
            ++quantidadeVitorias;
        saida.writeByte(CODIGO_VENCEDOR);
        saida.writeInt(id);
        saida.flush();
    }

    public void novaRodada() throws IOException {
        saida.writeByte(NOVA_RODADA);
        saida.flush();
    }

    public int getId() {
        return ID;
    }

    public String getRemoteAddress() {
        return  socket.getRemoteSocketAddress().toString();
    }

    @Override
    public String toString() {
        return "Jogador " + ID;
    }    
}