package src.server;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.table.DefaultTableModel;
import src.comum.Par;

import static src.server.TiposEventos.*;
import static java.lang.String.format;

public class ChangeListener implements PropertyChangeListener {

    private Gui gui;
    private String ultimoEvento = "0";
    private final String NOVA_LINHA = System.getProperty("line.separator");
    private int indiceLinhaAtual = 0;
    private int quantidadeConexoes = 0;
    private int contagemTotal = 0;

    public ChangeListener(Gui gui) {
        this.gui = gui;
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        String nome = evt.getPropertyName();
        JogadorRemoto jogador;
        switch(nome) {
            case AGUARDANDO:
                quantidadeConexoes = (int)evt.getNewValue();
                setMensagem(format("Aguardando %d conexões...", quantidadeConexoes));
                gui.getButtonDesconectar().setEnabled(true);
                break;
            case CONECTADO:
                Par par = (Par)evt.getNewValue();
                indiceLinhaAtual = 0;                
                gui.setTitle("Servidor - conectado");
                setPorta(par.valor2.toString());
                appendLog("Servidor %s ouvindo na porta %s", par.valor1, par.valor2);                
                break;
            case CONECTANDO:
                setLog("Conectando-se na porta %s", evt.getNewValue());
                break;
            case ERRO:
                setLog("%s", evt.getNewValue());
                break;
            case ERRO_AO_CONECTAR:
                setLog("Erro ao conectar: %s", evt.getNewValue());
                gui.getButtonIniciar().setEnabled(true);
                break;
            case ERRO_AO_CRIAR_JOGADORES:
                setLog("Não foi possível criar os jogadores");
                gui.setTitle("Servidor - desconectado");
                gui.getButtonIniciar().setEnabled(true);
                setMensagem("");
                break;
            case ERRO_COMUNICACAO:
                jogador = (JogadorRemoto)evt.getNewValue();
                setLog("Não foi possível se comunicar com %s", jogador);
                break;
            case FIM_JOGO:
                appendLog("Fim de jogo");
                setMensagem("");
                limparTabela();
                setTotal("-");
                gui.setTitle("Servidor - desconectado");
                gui.getButtonIniciar().setEnabled(true);
                gui.getButtonNovaRodada().setEnabled(false);
                gui.getButtonDesconectar().setEnabled(false);                
                break;
            case FIM_RODADA:
                gui.getButtonNovaRodada().setEnabled(true);
                if(ultimoEvento != FIM_JOGO)
                    proximaLinha();                    
                break;
            case INICIO_RODADA:
                contagemTotal = 0;
                setTotal("-");
                setMensagem("");
                resetarValoresTabela();        
                selecionarLinha();
                break;
            case JOGADOR_CRIADO:
                setMensagem(format("Aguardando %d conexões...", --quantidadeConexoes));
                if(quantidadeConexoes == 0)
                    setMensagem("");
                inserirJogador((JogadorRemoto)evt.getNewValue());
                break;
            case NOVA_CONTAGEM:
                jogador = (JogadorRemoto)evt.getNewValue();
                inserirContagem(jogador.getUltimaContagem());
                proximaLinha();
                selecionarLinha();
                break;
            case NOVO_PALPITE:
                jogador = (JogadorRemoto)evt.getNewValue();
                inserirPalpite(jogador.getUltimoPalpite());
                proximaLinha();
                selecionarLinha();
                break;
            case SEM_VENCEDOR:
                setMensagem("Ninguém venceu");
                break;
            case VENCEDOR:
                jogador = (JogadorRemoto)evt.getNewValue();
                setMensagem(format("Vence %s", jogador));
                inserirVitoria(jogador);
                break;
            default:
                break;
        }
        ultimoEvento = nome;
    }

    private void setMensagem(String msg) {
        gui.getLabelMensagem().setText(msg);
    }

    private void appendLog(String formato, Object... args) {
        gui.getTextAreaLog().append(NOVA_LINHA + format(formato, args));
    }

    private void setLog(String formato, Object... args) {
        gui.getTextAreaLog().setText(format(formato, args));
    }

    private void setTotal(String valor) {
        gui.getTextFieldContagemTotal().setText(valor);
    }

    private void setTotal(int valor) {
        gui.getTextFieldContagemTotal().setText(Integer.toString(valor));
    }

    private void limparTabela() {
        gui.getTableModel().setRowCount(0);
    }

    private void inserirJogador(JogadorRemoto jogador) {
        gui.getTableModel().addRow(new Object[]{ jogador.getId(), "-", "-", 0, jogador.getRemoteAddress() });
    }

    private void inserirContagem(int valor) {        
        gui.getTableModel().setValueAt(valor, indiceLinhaAtual, 2);
        contagemTotal += valor;
        setTotal(contagemTotal);
    }

    private void inserirPalpite(int valor) {
        gui.getTableModel().setValueAt(valor, indiceLinhaAtual, 1);
    }

    private void proximaLinha() {
        indiceLinhaAtual = ++indiceLinhaAtual % gui.getTableModel().getRowCount();
    }

    private void selecionarLinha() {
        gui.getTableJogadores().setRowSelectionInterval(indiceLinhaAtual, indiceLinhaAtual);
    }

    private void inserirVitoria(JogadorRemoto jogador) {
        gui.getTableModel().setValueAt(jogador.getQuantidadeVitorias(), jogador.getId() - 1, 3);
    }

    private void setPorta(String valor) {
        gui.getTextFieldPorta().setText(valor);
    }

    private void resetarValoresTabela() {
        DefaultTableModel tableModel = gui.getTableModel();
        int rowCount = gui.getTableModel().getRowCount();
        for(int i = 0; i < rowCount; ++i) {
            tableModel.setValueAt("-", i, 1);
            tableModel.setValueAt("-", i, 2);
        }
    }
}