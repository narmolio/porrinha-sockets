package src.server;

import java.io.IOException;
import java.util.LinkedList;

public class Jogo {
    
    private EventosJogo eventos;
    private Servidor servidor;
    private LinkedList<JogadorRemoto> jogadores = new LinkedList<>();
    private int[] palpitesPossiveis;
    private boolean jogoAtivo = false;
    private JogadorRemoto ultimoJogador = null;

    private final int CONTAGEM_INDIVIDUAL_MAXIMA = 3;

    public Jogo(EventosJogo eventos, Servidor servidor) {
        this.eventos = eventos;
        this.servidor = servidor;
    }

    public void criarJogadores(int quantidade) throws IOException {
        int id = 1;
        eventos.aguardando(quantidade);
        while(0 != quantidade--) {
            JogadorRemoto jogador = new JogadorRemoto(servidor.aceitarConexao(), id++);
            jogadores.add(jogador);
            eventos.jogadorCriado(jogador);
        }
        desconectarServidor();
    }

    public void jogarRodada() {
        if(jogadores.size() == 0) {
            eventos.erro("Não há jogadores");
            return;
        }
        jogoAtivo = true;
        eventos.inicioRodada();        
        try {                  
            alertarNovaRodada();
            int[] palpites = getPalpiteJogadores();
            int total = getContagemTotal(palpites);
            eventos.total(total);
            JogadorRemoto vencedor = getVencedor(total);
            if(vencedor != null) {                
                eventos.vencedor(vencedor);
                alertarVencedor(vencedor.getId());
            }            
            else {
                eventos.semVencedor();
                alertarVencedor(-1);
            }            
            jogadores.add(jogadores.pollFirst());
            eventos.fimRodada();
        }
        catch(IOException e) {
            encerrarConexoes();
            jogoAtivo = false;
            eventos.erroComunicacao(ultimoJogador);
            eventos.fimDeJogo();
        }
        catch(Exception e) {            
            encerrarConexoes();
            jogoAtivo = false;
            eventos.erro(e.getMessage());
            eventos.fimDeJogo();
        }
        jogoAtivo = false;
    }

    public void terminar() {
        if(jogadores.size() == 0) {
            desconectarServidor();
            return;
        }            
        if(!jogoAtivo) {
            encerrarConexoes();
            eventos.fimDeJogo();
        }
        jogoAtivo = false;
    }

    private void alertarNovaRodada() throws IOException {
        for (JogadorRemoto jogador: jogadores) {
            jogador.novaRodada();
        }
    }

    private void alertarVencedor(int id) throws IOException, Exception {
        for(JogadorRemoto jogador: jogadores) {
            if(!jogoAtivo)
                throw new Exception("jogo interrompido");
            ultimoJogador = jogador;
            jogador.alertarVencedor(id);                
        }                
    }    

    private int getContagemTotal(int[] palpites) throws Exception, IOException {
        int total = 0;            
        for(JogadorRemoto jogador: jogadores) {
            if(!jogoAtivo)
                throw new Exception("jogo interrompido");
            ultimoJogador = jogador;
            int contagem = jogador.getContagem(jogadores.size(), palpites);
            total += contagem;
            eventos.novaContagem(jogador);
        }
        return total;
    }


    private int[] getPalpiteJogadores() throws Exception, IOException {
        int[] palpites = getPalpitesPossiveis();
        int palpitesFornecidos = 0;
        for(JogadorRemoto jogador: jogadores) {
            if(!jogoAtivo)
                throw new Exception("Jogo interrompido");
            ultimoJogador = jogador;
            int palpite = jogador.getPalpite(palpitesFornecidos, palpites);
            int indice = getIndicePalpite(palpites, palpitesFornecidos, palpite);
            if(indice == -1)
                throw new Exception(String.format("O palpite %d fornecido por %s é invalido", palpite, jogador));                
            troca(palpites, indice, palpitesFornecidos);
            palpitesFornecidos++;
            eventos.novoPalpite(jogador);
        }
        return palpites;
    }

    private int getIndicePalpite(int[] palpites, int palpitesFornecidos, int palpite) {
        for(int i = palpitesFornecidos; i < palpites.length; ++i)
            if(palpites[i] == palpite)
                return i;
        return -1;
    }

    private int[] getPalpitesPossiveis() {
        int palpiteMaximo = jogadores.size() * CONTAGEM_INDIVIDUAL_MAXIMA + 1;
        if(this.palpitesPossiveis != null && this.palpitesPossiveis.length == palpiteMaximo)
            return this.palpitesPossiveis;
        this.palpitesPossiveis = new int[palpiteMaximo];        
        for(int i = 0; i < palpiteMaximo; ++i)
            this.palpitesPossiveis[i] = i;
        return this.palpitesPossiveis;
    }

    private void troca(int[] palpites, int indice1, int indice2) {
        int valor1 = palpites[indice1];
        palpites[indice1] = palpites[indice2];
        palpites[indice2] = valor1;
    }


    private JogadorRemoto getVencedor(int total) {
        for(JogadorRemoto jogador: jogadores)
            if(jogador.getUltimoPalpite() == total)
                return jogador;
        return null;
    }

    private void encerrarConexoes() {
        for(JogadorRemoto jogador: jogadores) {
            try { jogador.encerrarConexao(); }
            catch(Exception e){ }
        }
        jogadores.clear();        
    }

    private void desconectarServidor() {
        try {
            servidor.desconectar();
        }
        catch(Exception e) {}
    }
}