package src.server;

import javax.swing.SwingWorker;
import static src.server.TiposEventos.ERRO_AO_CONECTAR;
import static src.server.TiposEventos.ERRO_AO_CRIAR_JOGADORES;;

public class Worker extends SwingWorker<Void, Void> {

    private Jogo jogo;
    private Servidor servidor;
    private int porta;
    private int quantidadeJogadores;
    private boolean necessitaConexao;

    public Worker(Jogo jogo, Servidor servidor, int porta, int quantidadeJogadores) {
        this.jogo = jogo;
        this.servidor = servidor;
        this.porta = porta;
        this.quantidadeJogadores = quantidadeJogadores;
        necessitaConexao = true;
    }

    public Worker(Jogo jogo) {
        this.jogo = jogo;
        necessitaConexao = false;
    }

    @Override
    public Void doInBackground() throws Exception {
        if(necessitaConexao) {
            if(conectar() && criarJogadores())
                jogo.jogarRodada();
        }
        else
            jogo.jogarRodada();
        return null;
    }

    private boolean conectar() {
        try {
            servidor.connectar(porta);
            return true;
        }
        catch(Exception e) {
            firePropertyChange(ERRO_AO_CONECTAR, null, e.getMessage());
        }
        return false;
    }

    private boolean criarJogadores() {
        try {
            jogo.criarJogadores(quantidadeJogadores);
            return true;
        }
        catch(Exception e) {
            firePropertyChange(ERRO_AO_CRIAR_JOGADORES, null, "erro");
        }
        return false;
    }

}