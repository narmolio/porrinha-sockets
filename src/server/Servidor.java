package src.server;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class Servidor {    

    private ServerSocket socketServidor = null;
    private EventosServidor eventos;

    public Servidor(EventosServidor eventos) {
        this.eventos = eventos;
    }
    
    public void connectar(int porta) throws IOException {
        eventos.conectando(porta);
        desconectar();
        socketServidor = new ServerSocket(porta);
        eventos.conectado(InetAddress.getLocalHost().getHostAddress(), socketServidor.getLocalPort());
    }

    public void desconectar() throws IOException {
        if(socketServidor != null) {
            socketServidor.close();
            socketServidor = null;
            eventos.desconectado();
        }            
    }

    public Socket aceitarConexao() throws IOException {
        if(socketServidor == null)
            connectar(0);
        return socketServidor.accept();
    }

}