package src.server;

public abstract class TiposEventos {
    public static final String ERRO_AO_CONECTAR = "a";
    public static final String ERRO_AO_CRIAR_JOGADORES = "b";
    public static final String CONECTANDO = "c";
    public static final String CONECTADO = "d";
    public static final String DESCONECTADO = "e";
    public static final String JOGADOR_CRIADO = "f";
    public static final String ERRO_COMUNICACAO = "g";
    public static final String AGUARDANDO = "h";
    public static final String ERRO = "i";
    public static final String NOVO_PALPITE = "j";
    public static final String NOVA_CONTAGEM = "k";
    public static final String TOTAL = "l";
    public static final String VENCEDOR = "m";
    public static final String SEM_VENCEDOR = "n";
    public static final String INICIO_RODADA = "o";
    public static final String FIM_RODADA = "p";
    public static final String FIM_JOGO = "q";
}