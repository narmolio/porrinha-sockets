package src.server;

public interface EventosServidor {
    public void conectando(int porta);
    public void conectado(String host, int porta);
    public void desconectado();
}