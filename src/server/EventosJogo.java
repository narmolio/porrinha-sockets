package src.server;

public interface EventosJogo {
    public void jogadorCriado(JogadorRemoto jogador);
    public void erroComunicacao(JogadorRemoto jogador);
    public void aguardando(int quantidade);
    public void erro(String msg);
    public void novoPalpite(JogadorRemoto jogador);
    public void novaContagem(JogadorRemoto jogador);
    public void total(int valor);
    public void vencedor(JogadorRemoto jogador);
    public void semVencedor();
    public void inicioRodada();
    public void fimRodada();
    public void fimDeJogo();
}