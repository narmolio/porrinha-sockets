package src.server;

import src.comum.Par;
import static src.server.TiposEventos.*;

public class Eventos implements EventosJogo, EventosServidor {

    private Worker worker;

    public void setSwingWorker(Worker worker) {
        this.worker = worker;
    }

    @Override
    public void conectando(int porta) {
        worker.firePropertyChange(CONECTANDO, null, porta);
    }

    @Override
    public void conectado(String host, int porta) {
        worker.firePropertyChange(CONECTADO, null, new Par(host, porta));

    }

    @Override
    public void desconectado() {
        worker.firePropertyChange(DESCONECTADO, null, "desconectado");

    }

    @Override
    public void jogadorCriado(JogadorRemoto jogador) {
        worker.firePropertyChange(JOGADOR_CRIADO, null, jogador);
    }

    @Override
    public void erroComunicacao(JogadorRemoto jogador) {
        worker.firePropertyChange(ERRO_COMUNICACAO, null, jogador);
    }

    @Override
    public void aguardando(int quantidade) {
        worker.firePropertyChange(AGUARDANDO, null, quantidade);
    }

    @Override
    public void erro(String msg) {
        worker.firePropertyChange(ERRO, null, msg);
    }

    @Override
    public void novoPalpite(JogadorRemoto jogador) {
        worker.firePropertyChange(NOVO_PALPITE, null, jogador);

    }

    @Override
    public void novaContagem(JogadorRemoto jogador) {
        worker.firePropertyChange(NOVA_CONTAGEM, null, jogador);
    }

    @Override
    public void total(int valor) {
        worker.firePropertyChange(TOTAL, null, valor);
    }

    @Override
    public void vencedor(JogadorRemoto jogador) {
        worker.firePropertyChange(VENCEDOR, null, jogador);
    }

    @Override
    public void semVencedor() {
        worker.firePropertyChange(SEM_VENCEDOR, null, "sem vencedor");
    }

    @Override
    public void inicioRodada() {
        worker.firePropertyChange(INICIO_RODADA, null,  "inicio_rodada");
    }

    @Override
    public void fimRodada() {
        worker.firePropertyChange(FIM_RODADA, null, "fim rodada ");

    }

    @Override
    public void fimDeJogo() {
        worker.firePropertyChange(FIM_JOGO, null, "fim jogo");
    }
    
}