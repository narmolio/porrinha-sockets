package src.server;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import javax.swing.table.DefaultTableCellRenderer;

public class Gui extends JFrame {

    private static final long serialVersionUID = 1L;

    private class JogadoresTableModel extends DefaultTableModel {

        private static final long serialVersionUID = 1L;

        public JogadoresTableModel(Object[] columnNames, int rowCount) {
            super(columnNames, rowCount);
        }

        @Override
        public boolean isCellEditable(int row, int column) {
            return false;
        }
        
    }

    private final JTextField textFieldPorta = new JTextField(5);
    private final JTextField textFieldQuantidadeJogadores = new JTextField(3);
    private final JTextField textFieldContagemTotal = new JTextField("-", 3);
    private final JButton buttonIniciar = new JButton("Iniciar");
    private final JButton buttonNovaRodada = new JButton("Nova rodada");
    private final JButton buttonDesconectar = new JButton("Desconectar");
    private final JLabel labelMensagem = new JLabel();
    private final Object[] colunas = { "Jogador", "Palpite", "Contagem", "Vitórias", "Endereço" };
    private final DefaultTableModel tableModel = new JogadoresTableModel(colunas, 0);
    private final JTable tableJogadores = new JTable(tableModel);
    private final JTextArea textAreaLog = new JTextArea(3, 20);

    public Gui() {
        iniciar();
    }

    public void iniciar() {
        setResizable(false);
        criarComponentes();
    }

    private void criarComponentes() {
        Container contentPane = getContentPane();
        JPanel raiz = new JPanel();
        JPanel panel1 = new JPanel();
        JPanel panel2 = new JPanel();
        JPanel panel3 = new JPanel();
        JPanel panel4 = new JPanel();
        JLabel labelPorta = new JLabel("Porta");
        JLabel labelQuantidadeJogadores = new JLabel("Quantidade de jogadores");
        JLabel labelContagemTotal = new JLabel("Contagem total");
        JScrollPane paneTabela = new JScrollPane(tableJogadores);
        Dimension tamanhoTabela = new Dimension(400, 150);
        Border bordaExternaTextArea = BorderFactory.createEtchedBorder();
        Border bordaInternaTextArea = BorderFactory.createEmptyBorder(5, 5, 5, 5);
        Border bordaTextArea = BorderFactory.createCompoundBorder(bordaExternaTextArea, bordaInternaTextArea);
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        TableColumnModel columnModel = tableJogadores.getColumnModel();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        columnModel.getColumn(0).setCellRenderer(centerRenderer);
        columnModel.getColumn(1).setCellRenderer(centerRenderer);
        columnModel.getColumn(2).setCellRenderer(centerRenderer);
        columnModel.getColumn(3).setCellRenderer(centerRenderer);
        columnModel.getColumn(4).setCellRenderer(centerRenderer);
        columnModel.getColumn(4).setPreferredWidth(130);
        tableJogadores.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        tableJogadores.getTableHeader().setReorderingAllowed(false);
        paneTabela.setMaximumSize(tamanhoTabela);
        paneTabela.setPreferredSize(tamanhoTabela);
        textFieldContagemTotal.setEditable(false);
        textFieldContagemTotal.setHorizontalAlignment(JTextField.CENTER);
        textAreaLog.setMaximumSize(new Dimension(390, 250));
        textAreaLog.setBorder(bordaTextArea);
        textAreaLog.setEditable(false);
        textAreaLog.setForeground(Color.BLUE);
        panel1.add(labelPorta);
        panel1.add(textFieldPorta);
        panel1.add(labelQuantidadeJogadores);
        panel1.add(textFieldQuantidadeJogadores);
        panel2.add(buttonIniciar);
        panel2.add(buttonNovaRodada);
        panel2.add(buttonDesconectar);
        panel3.add(labelMensagem);
        panel4.add(labelContagemTotal);
        panel4.add(textFieldContagemTotal);        
        raiz.setLayout(new BoxLayout(raiz, BoxLayout.Y_AXIS));
        raiz.add(panel1);
        raiz.add(panel2);
        raiz.add(panel3);
        raiz.add(textAreaLog);
        raiz.add(Box.createRigidArea(new Dimension(0, 5)));
        raiz.add(paneTabela);
        raiz.add(panel4);    
        contentPane.add(raiz, BorderLayout.CENTER);
        pack();
    }

    public void exibir() {
        setVisible(true);
    }

    public JTextField getTextFieldPorta() {
        return textFieldPorta;
    }

    public JTextField getTextFieldQuantidadeJogadores() {
        return textFieldQuantidadeJogadores;
    }

    public JButton getButtonIniciar() {
        return buttonIniciar;
    }

    public JButton getButtonNovaRodada() {
        return buttonNovaRodada;
    }

    public JButton getButtonDesconectar() {
        return buttonDesconectar;
    }

    public JLabel getLabelMensagem() {
        return labelMensagem;
    }

    public JTable getTableJogadores() {
        return tableJogadores;
    }

    public DefaultTableModel getTableModel() {
        return tableModel;
    }

    public JTextField getTextFieldContagemTotal() {
        return textFieldContagemTotal;
    }

    public JTextArea getTextAreaLog() {
        return textAreaLog;
    }

}