package src.server;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import java.awt.event.ActionEvent;

public class App implements Runnable {

    private final String PORTA_PADRAO = "42426";
    private final String QUANTIDADE_JOGADORES_PADRAO = "3";

    private Gui gui;
    private Eventos eventos = new Eventos();
    private Servidor servidor = new Servidor(eventos);
    private Jogo jogo = new Jogo(eventos, servidor);
    private ChangeListener changeListener;
    private Worker worker = null;

    @Override
    public void run() {
        gui = new Gui();
        changeListener = new ChangeListener(gui);

        gui.setTitle("Servidor - desconectado");
        gui.getTextFieldPorta().setText(PORTA_PADRAO);
        gui.getTextFieldQuantidadeJogadores().setText(QUANTIDADE_JOGADORES_PADRAO);
        gui.getButtonNovaRodada().setEnabled(false);
        gui.getButtonDesconectar().setEnabled(false);

        gui.getButtonIniciar().addActionListener((ActionEvent evt) -> {
            try {
                if(worker == null || worker.isDone()) {
                    worker = new Worker(jogo, servidor, getPorta(), getQuantidadeJogadores());
                    eventos.setSwingWorker(worker);
                    worker.addPropertyChangeListener(changeListener);
                    worker.execute();
                    gui.getButtonIniciar().setEnabled(false);
                }    
            }
            catch(Exception e) {
                gui.getTextAreaLog().setText(e.getMessage());
                gui.getButtonIniciar().setEnabled(true);
            }
        });

        gui.getButtonNovaRodada().addActionListener((ActionEvent e) -> {
           if(worker == null || worker.isDone()) {
               worker = new Worker(jogo);
               eventos.setSwingWorker(worker);
               worker.addPropertyChangeListener(changeListener);
               worker.execute();
               gui.getButtonNovaRodada().setEnabled(false);
           }
        });

        gui.getButtonDesconectar().addActionListener((ActionEvent e) -> {
            gui.getButtonDesconectar().setEnabled(false);
            jogo.terminar();
        });

        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                jogo.terminar();
            }
        });

        gui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        gui.exibir();

    }

    public void iniciar() {
        SwingUtilities.invokeLater(this);
    }

    private int getPorta() throws Exception {
        Exception exception = new Exception("'Porta' precisa ser um inteiro no intervalo [0, 65535]");
        try {
            String text = gui.getTextFieldPorta().getText().trim();
            if(text.isEmpty())
                return 0;
            int valor = Integer.parseInt(text);
            if(valor < 0 || valor > 65535)
                throw exception;
            return valor;    
        }
        catch(NumberFormatException e) {
            throw exception;
        }
    }

    private int getQuantidadeJogadores() throws Exception {
        Exception exception = new Exception("'Quantidade de jogadores' precisa ser um valor maior que 1");
        try {
            String text = gui.getTextFieldQuantidadeJogadores().getText().trim();
            if(text.isEmpty())
                throw exception;
            int valor = Integer.parseInt(text);
            if(valor < 2)
                throw exception;
            return valor;
        }
        catch(NumberFormatException e) {
            throw exception;
        }

    }

}